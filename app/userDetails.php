 <!Doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="$1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../view/css/style.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

        <title>SEND MESSAGE</title>
       
    </head>
        <body class="pagebody">
             <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand" href="#">Skychat</a>
                </div>
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li><a href="#">Page 1</a></li>
                  <li><a href="#">Page 2</a></li>
                  <li><a href="#">Page 3</a></li>
                </ul>
              </div>
            </nav>
            <div class ="mybody">
                 <?php
                    session_start();
                    echo "<label class =\"header\">" .  $_SESSION["email"] . "</label>" ;
                  ?>
                <hr>
                <br/><br/>
                <label id="first"> Send To:</label><br/>
                <form action="message.php" method="post" id="form" class="myform"> 
                <br/>    
                <select name ="users" id="users" class="myselect">
                    <?php
                        require_once("dbconfig.php");
                        $sql =" Select user_id from user where email !='".$_SESSION["email"]."'";
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_array())
                        {
                            echo "<option value=\"" .$row['user_id']."\" >" .$row['user_id']. "</option>";

                        }
                    ?>
                 
                </select>
                    <br/><br/>
                
                <textarea maxlength="5000" id="content" name="content" class= "mytext" placeholder="Write Message here"></textarea>
                    <br/><br/>
                <input type="submit" id="save" value="Send Message"> 
            </form>
            </div>
             <footer>
                <div class="myfooter" style="padding: 2px;  border-radius: 1px; ">
                
                    <div class="mycontainer">
                        <p style="margin-left:11%;">
                         Find us on:
                        </p>
                        
                        <a href="https://www.facebook.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                        <a href="https://twitter.com/bootsnipp"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                        <a href="https://plus.google.com/+Bootsnipp-page"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                        <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                    
                    
                    </div>
            </div>
        </footer>
            
            <!--<script src="../view/js/userNewMessage.js"></script>-->
            <script src="../view/js/userDetails.js"></script>
        </body>
    </html>