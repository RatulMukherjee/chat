<?php
    session_start();
     if ($_SESSION["email"] == NULL && $_SESSION["user_id"] == NULL)
                    {
                        header("Location:userLogin.html");
                        return;
                    }

?>
<!Doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="$1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
 
        <title>LOGOUT</title>
    </head>
        <body class="pagebody">
            <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand" href="#">Skychat</a>
                </div>
                <ul class="nav navbar-nav">
                  <li class="active"><a href="userLogin.html">Login</a></li>
                  
                </ul>
              </div>
            </nav>
            <div class="mybody">
                <?php
    
                    echo "Logged out successfully " . $_SESSION['email'];
                    unset($_SESSION['email']);
                    unset($_SESSION['user_id']);

                ?>
            </div>
          <!--  <footer>
                <div class="myfooter">
                
                    <div class="mycontainer">
                        <p style="margin-left:11%;">
                         Find us on:
                        </p>
                        
                        <a href="https://www.facebook.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                        <a href="https://twitter.com/bootsnipp"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                        <a href="https://plus.google.com/+Bootsnipp-page"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                        <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                    
                    
                    </div>
            </div>
        </footer>-->
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
            <script>
    $(document).ready(function(){
        
        /*console.log(localStorage.getItem('user_id'));*/
       
        localStorage.clear();
        
       /* console.log(localStorage.getItem('user_id'));*/
    });
</script>
        </body>
    </html>
