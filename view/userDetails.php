<?php session_start();
        if ($_SESSION["email"] == NULL && $_SESSION["user_id"] == NULL )
                    {
                        header("Location:userLogin.html");
                        return;
                    }?>
<!Doctype html>
    <html>
        
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="$1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        


        <title>SEND MESSAGE</title>
       
    </head>
        <body class="pagebody">
             <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand" href="#">Skychat</a>
                </div>
                <ul class="nav navbar-nav">
                  <li id="home" class="active"><a href="#">Home</a></li>
                  <li><a id="messages" href="userMessages.html">Sent</a></li>
                  <li><a id="inbox" href="userInbox.html">Inbox&nbsp;<span class="glyphicon glyphicon-envelope" id= "count" style="color: white;"> </span></a></li>
                  <!--<li><span class="glyphicon glyphicon-envelope" id= "count" style="color: white; padding: 12px; "> </span></li>--> 
                  <li><a href="Logout.php">Logout</a></li>
                  
                </ul>
              </div>
            </nav>
            <div class ="mybody">
                 <?php
                    
                    echo "<label class =\"header\">" .  $_SESSION["email"] . "</label>" ;
                  ?>
                <hr>
                <br/><br/>
                <label id="first"> Send To:</label><br/>
                <form action="../app/message.php" method="post" id="form" class="myform"> 
                <br/>    
                <select name ="users" id="users" class="myselect">
                    <?php
                        require_once("../app/dbconfig.php");
                        $sql =" Select user_id from user where email !='".$_SESSION["email"]."'";
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_array())
                        {
                            echo "<option value=\"" .$row['user_id']."\" >" .$row['user_id']. "</option>";

                        }
                    ?>
                 
                </select>
                    <br/><br/>
                
                <textarea maxlength="5000" id="content" name="content" class= "mytext" placeholder="Write Message here"></textarea>
                    <br/><br/>
                <input type="submit" id="save" value="Send Message"> 
            </form>
            </div>
              <footer>
                <div class="myfooter">
                
                    <div class="mycontainer">
                        <p style="margin-left:11%;">
                         Find us on:
                        </p>
                        
                        <a href="https://www.facebook.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                        <a href="https://twitter.com/bootsnipp"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                        <a href="https://plus.google.com/+Bootsnipp-page"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                        <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                    
                    
                    </div>
            </div>
        </footer>
       
            
            
            <script src="js/userDetails.js"></script>
        </body>
    </html>